#!/usr/bin/env python3
from skbuild import setup  # This line replaces 'from setuptools import setup'
import setuptools
setup(
    name="coupy",
    version="0.0.1",
    author="Bas van 't Hof for Leiden University",
    author_email="vanthofbas@gmail.com",
    description="Coupling C++ functions and classes to Python",
    url="https://gitlab.com/Bas.vanthof/c-oupy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
      "autopep8",
      "TsetSE",
    ],
    scripts = ['coupy/C++ouPy.py'],
    include_package_data=True,
    package_data={
            "": ["doc/*.pdf", "lib/*.so", "include/*.h"]},

)
