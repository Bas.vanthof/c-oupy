#include <stdio.h>
#include <stdio.h>
#include <stdarg.h>
#include "C++ouPy.h"

/* The (latest) error message is stored in a static string */
const size_t n_err_msg=1000;
static char err_msg[n_err_msg];

/* Copy one string into the other. Truncate at out_size, and
   make sure the output string ends with a null character */
static char* strncpy_t(char* out, const char* in, size_t out_size){
    for (size_t i =0; i<out_size; i++) {
        out[i] = in[i];
        if (out[i]==(char) 0) break;
    }
    out[out_size-1] = (char)0;
    return out;
}

/* Copy the (latest) error message into the output string */
void get_error(char *string) {
    strncpy_t(string, err_msg, strlen(string));
 }

/* Copy the input string to the (latest) error message.
   The standard output is flushed because this typically happens
   when C++ code is finished and Python starts again.
   C++ and Python buffer their standard out differently, so it
   comes out in the wrong order if there is no fflush */
void set_error(const char *string) {
    fflush(stdout);
    strncpy_t(err_msg, string, n_err_msg);
 }

static FILE *logfile;

int tflush() {
    fflush(stdout);
    fflush(stderr);
    if (logfile) fflush(logfile);
    return 1;
}

int open_logfile(const char fname[]) {
    if (logfile) fclose(logfile);
    logfile=fopen(fname,"a");
    return 1;
};

int close_logfile() {
    if (logfile) fclose(logfile);
    logfile = NULL;
    return 1;
};

int tprint(const char *str) {
    int rc = printf("%s",str);
    if(rc >= 0 && logfile) rc = fprintf(logfile, "%s", str);
    return rc;
}

int tprintf(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    int rc = vprintf(fmt, ap);
    va_end(ap);

    if(rc >= 0 && logfile) {
        va_start(ap, fmt);
        rc = vfprintf(logfile, fmt, ap);
        va_end(ap);
    }
    return rc;
}
