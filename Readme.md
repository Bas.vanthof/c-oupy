# C++ouPy

C++ouPy (pronounce choopey) is a system that makes C++ classes and functions available to python, allowing you to combine the speed of C++ code and the flexibility of python. 

# Installation

__C++ouPy__ is installed automatically as a _submodule_ of __CapSys__. 
When installing __CapSys__, it is not necessary to also install __C++ouPy__.

# Manual
The manual for __C++ouPy__ is found in __pdfs/C++ouPy_manual.pdf__

