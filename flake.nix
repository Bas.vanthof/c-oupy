{
  description = "C++oupy";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    TsetSE = {
      url = "git+https://gitlab.com/Bas.vanthof/tsetse.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, TsetSE }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        pkgs = import nixpkgs { inherit system; };

        nativeBuildInputs = [
          pkgs.cmake
          pkgs.python3Packages.scikit-build
          pkgs.texlive.combined.scheme-small
        ];

        propagatedBuildInputs = [
          pkgs.python3Packages.autopep8
          TsetSE.packages.${system}.TsetSE
        ];

        checkInputs = [ 
           pkgs.python3Packages.pytest
           #pkgs.python3Packages.pytestCheckHook 
        ];

        C-ouPy =
          pkgs.python3Packages.buildPythonPackage rec {
            pname = "coupy";
            version = "0.0.1";
            src = builtins.path { path = ./.; name = pname; };

            inherit nativeBuildInputs propagatedBuildInputs checkInputs;
            pytestFlagsArray = [ "--verbose" ];

            checkPhase = ''(cd tests && python -m pytest --verbose)'';

            pythonImportsCheck = [ "coupy" ];
            dontConfigure = true;
          };

      in {

        packages = {
          inherit C-ouPy;
          default = C-ouPy;
        };

        devShells = {
          default = pkgs.mkShell {
            name = "C++ouPy-dev";
            venvDir = "./.venv";
            packages = [
              pkgs.python3Packages.venvShellHook
            ] ++ propagatedBuildInputs ++ [pkgs.python3Packages.pytest] ++ nativeBuildInputs;
          };
        };
      });
}
