import os
from TsetSE.GraphicsComparison import GraphicsComparison, PLOT_NEVER, pytest_init, set_graphics, ACCEPT_BLINDLY
from coupy.coupy_main import generate_couplings
from glob import glob
import pytest
import importlib
import json

pytest_init(__file__)
with open(os.path.join(os.path.dirname(__file__), 'testfile.json'), 'r') as fin:
    tests = json.load(fin)

@pytest.mark.parametrize("test", tests)
def test_coupy(test):
    lib = importlib.import_module(test)
    set_graphics(ACCEPT_BLINDLY) 
    lib.run()
    set_graphics(PLOT_NEVER) 
    lib.run()

generate_couplings(glob(os.path.join(os.path.dirname(__file__),'c*.json')), False)


