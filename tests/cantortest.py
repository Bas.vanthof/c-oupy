#!/usr/bin/env python3
from cantor import Cantor
from TsetSE.GraphicsComparison import GraphicsComparison

def plotfun(plotdata):
    """
    Plot function of test cantortest
    ================================
    This function plots blue and red trianles to viaualize the Cantor set.
    """
    import matplotlib.pyplot as plt
    for i, x in enumerate(plotdata['x']):
        y = plotdata['y'][i]
        c = plotdata['c'][i]
        plt.fill(x,y,c)
    plt.gca().set_aspect('equal')
    plt.axis('off')
    plt.suptitle('Visualization of Cantor set, %d times refined' % plotdata['levels'])

def run():
    n = 4
    c = Cantor(n_refinements=n)
    x, y, c = c.plot_contours()
    plotdata = { 'x': x, 'y': y, 'c':c, 'levels':n, 'plotfun':plotfun}
    GraphicsComparison('cantor',plotdata, d=2)
