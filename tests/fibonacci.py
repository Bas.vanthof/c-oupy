from generated.cFibonacciC import fibonacci, fibonaccies
from TsetSE.GraphicsComparison import GraphicsComparison

def plotfun(plotdata):
    """
    Plot function of test fibonacci
    ===============================
    This function plots a blue bar graph for the Fibonacci numbers,
    and marks a selected one (the 13th) with a red dot.
    """
    import matplotlib.pyplot as plt
    i = plotdata['i']
    fibo_i = plotdata['fibo_i']
    fibos = plotdata['fibos']
    plt.bar(range(fibos.size),fibos)
    h = plt.plot(i,fibo_i,'r.', markersize=12)
    plt.suptitle('Fibonacci numbers')
    plt.xlabel('index i')
    plt.ylabel('fibonacci(i)')

def run():
    i = 13
    plotdata = { 'i': i, "fibo_i": fibonacci(i), 'fibos':fibonaccies(),
                 'plotfun': plotfun}
    GraphicsComparison('fibonacci',plotdata, d=2)
    

