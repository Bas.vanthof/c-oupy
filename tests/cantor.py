import numpy as np
from generated.CantorC import CantorC

class Cantor(CantorC):
    def __init__(self, max_interval_length=1, n_refinements=0):
        print("Calling CantorC's __init__")
        CantorC.__init__(self, max_interval_length, n_refinements)

    def plot_contours(self):
        start, end = self.get_intervals()
        sqrt3 = np.sqrt(3.0)
        x = [None] * (2*start.size)
        y = [None] * (2*start.size)
        c = [None] * (2*start.size)
        x[-1::-2] = [ [-0.5*s, 0.5*s, 0, -0.5*s] for s in start]
        x[-2::-2] = [ [-0.5*e, 0.5*e, 0, -0.5*e] for e in end]
        y[-1::-2] = [ [sqrt3*s/6, sqrt3*s/6, -sqrt3*s/3, sqrt3*s/6 ] for s in start]
        y[-2::-2] = [ [sqrt3*e/6, sqrt3*e/6, -sqrt3*e/3, sqrt3*e/6 ] for e in end]
        c[-1::-2] = [ 'b'  for s in start]
        c[-2::-2] = [ 'r' for e in end]
        return x, y, c
            
