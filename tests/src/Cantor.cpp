#include "Cantor.h"
#include <cstdio>
#include <stdexcept>
Cantor::Cantor() { init(); }
Cantor::Cantor(const Cantor &other) { copy(other);}
Cantor::Cantor(const double max_interval_length) {
   init();
   while (end[0] - start[0] > max_interval_length) refine();
}
Cantor::Cantor(const size_t n_refinements){
   init();
   for(size_t j=0; j<n_refinements; j++) refine();
}

Cantor::Cantor(const double max_interval_length, const size_t n_refinements) {
   init();
   for(size_t j=0; j<n_refinements; j++) refine();
   while (end[0] - start[0] > max_interval_length) refine();
}

Cantor::~Cantor() { delete[] start; delete[] end;}
Cantor& Cantor::operator=(const Cantor &other) {copy(other); return *this;}

size_t Cantor::get_n_intervals() const {return n_intervals;}

void Cantor::get_intervals(double start_out[], double end_out[]) const {
    for (size_t j =0; j<n_intervals; j++) {
        start_out[j] = start[j];
        end_out[j] = end[j];
    }
}

void Cantor::refine(){
    double *start_new = new double[2*n_intervals];
    double *end_new = new double[2*n_intervals];
    for (size_t j =0; j<n_intervals; j++) {
       start_new[2*j] = start[j];
       end_new[2*j]   = (2*start[j]+end[j])/3;
       start_new[2*j+1] = (start[j]+2*end[j])/3;
       end_new[2*j+1]   = end[j];
    }
    delete[] start;
    delete[] end;

    end = end_new;
    start = start_new;
    n_intervals *=2;
}

void Cantor::print() const {
    printf("\n");
    for (size_t j =0; j<n_intervals; j++) {
       printf("  interval %d: %0.3f:%0.3f\n", (int) j, start[j], end[j]);
    }
    printf("\n");
}

void Cantor::copy(const Cantor &other) {
   n_intervals = other.n_intervals;
   start = new double [n_intervals];
   end = new double [n_intervals];
   for (size_t j =0; j<n_intervals; j++) {start[j] = other.start[j]; end[j]=other.end[j];}
}

void Cantor::init() {
 n_intervals = 1; start = new double[1]; end = new double[1]; start[0] = 0; end[0] =1;
}

