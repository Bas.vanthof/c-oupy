#include "fibonacci.h"
#include <stdexcept>

const size_t ARRAY_SIZE =17;

size_t fibonacci(const int i) {
    if (i<0) throw std::runtime_error("negative index");
    size_t retval = 1, nextval = 1;
    for (int j=0; j<i; j++) {
        const size_t hlp = retval;
        if (nextval < retval) throw std::runtime_error("size_t overflow"); 
        retval = nextval; nextval += hlp;
    }
    return retval;
}

size_t array_size() {return ARRAY_SIZE;}

void fibonaccies(size_t fibo[]) {
    fibo[0] = 1; fibo[1] = 1;
    for (size_t j=1; j<ARRAY_SIZE-1; j++) {
        fibo[j+1] = fibo[j] + fibo[j-1];
        if (fibo[j+1] < fibo[j]) throw std::runtime_error("size_t overflow"); 
    }
}

