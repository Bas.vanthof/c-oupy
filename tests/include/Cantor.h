#ifndef CANTOR_H
#define CANTOR_H
#include <cstddef>
class Cantor {
    public:
        Cantor();
        Cantor(const Cantor &other);
        Cantor(const double max_interval_length);
        Cantor(const size_t n_refinements);
        Cantor(const double max_interval_length, const size_t n_refinements);
        ~Cantor();
        Cantor& operator=(const Cantor &other);
        size_t get_n_intervals() const;
        void get_intervals(double start_out[], double end_out[]) const;
        void refine();
        void print() const;
    private:
        void copy(const Cantor &other);
        void init();
        size_t n_intervals;
        double *start;
        double *end;
};
#endif
