#ifndef FIBONACCI_H
#define FIBONACCI_H
#include <cstddef>
size_t fibonacci(const int i);
size_t array_size();
void fibonaccies(size_t fibo[]);
#endif

