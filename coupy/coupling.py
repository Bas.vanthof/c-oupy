"""
This module provides the class Coupling, which establishes a connection between functions and
classes in C++ and corresponding functions and classes in Python.
"""
import os
import re
import sys
import platform
import json
from ctypes import c_char_p, CDLL
import ctypes
import autopep8
import numpy as np
from glob import glob
from coupy.classes import Class
from coupy.method import Method

def coupy_dir():
    return os.path.dirname(__file__)

sys.path.insert(1, coupy_dir())

stdlogger = None
errlogger = None

class Logger(object):

    def __init__(self, outfile = 'logfile.txt', redirect_stderr=False):
        self.log = open(outfile, "a")
        self._redirect_stderr = redirect_stderr
        if redirect_stderr:
            self.terminal = sys.__stderr__
            sys.stderr = self
        else:
            self.terminal = sys.__stdout__
            sys.stdout = self

    def __del__(self):
        if self._redirect_stderr:
            sys.stderr = sys.__stderr__
        else:
            sys.stdout = sys.__stdout__
        self.log.close()
   
    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        self.terminal.flush()
        self.log.flush()


def include_name():
    return os.path.join(os.path.dirname(__file__),'include')

def dll_dir():
    return os.path.join(os.path.dirname(__file__),'lib')

def dll_name():
    name = os.path.join(dll_dir(),'libC++ouPy.so').replace('\\','/')
    if platform.system() == 'Windows':
        name = re.sub('.so$','.dll', name)
    else:
        name = re.sub('.dll$','.so', name)
    return name

dlls = {}
def loaded_dll(name):
    global dlls
    if not os.path.isfile(name):
        print("dll '%s' does not yet exist" % name)
    elif name not in dlls:
        print("Opening dll '%s'" % name)
        dlls[name] = ctypes.cdll.LoadLibrary(name)
    return dlls[name]

def relative_from(path_a, path_b):
    """
    Return the path_a, made relative from path_b
    """
    path_a = os.path.normpath(os.path.abspath(path_a)).split(os.sep)
    path_b = os.path.normpath(os.path.abspath(path_b)).split(os.sep)
    while path_a[0] == path_b[0]:
        path_a = path_a[1:]
        path_b = path_b[1:]
    path_a = ['..' for name in path_b] + path_a
    retval= os.path.join(*path_a)
    return retval.replace('\\','/')

def is_c_string(string):
    return type(string) == bytes and string[-1] == 0

def is_ndarray(array, dtype):
    """
    Check whether argument is an ndarray of the given data type,
    and that it is C-contiguous
    """
    return isinstance(array, np.ndarray) and \
        array.dtype == np.zeros(1, dtype=dtype).dtype and \
        array.flags["C_CONTIGUOUS"]

def close_logfile():
    """
    Close the logfile: 
    tprintf will print only to stdout from now on
    """
    coupy_dll.close_logfile.argtypes = []
    coupy_dll.close_logfile.restype = int
    stdlogger = None
    errlogger = None
    return coupy_dll.close_logfile()

def open_logfile(filenam):
    """
    Open a logfile:
    function tprintf will print to stdout AND to the logfile if there is one.
    """
    coupy_dll.open_logfile.argtypes = [c_char_p]
    coupy_dll.open_logfile.restype = int
    stdlogger = Logger(filenam)
    errlogger = Logger(filenam, redirect_stderr=True)
    return coupy_dll.open_logfile((filenam+'\0').encode('utf-8'))

def dll_runtime_error():
    """
    Look up the latest C++ouPy error message stored in the DLL, and
    raise a RuntimeError with that message
    """
    errmsg = (" "*5000 + "\0").encode("utf-8")
    coupy_dll.get_error.argtypes = [c_char_p]
    coupy_dll.get_error(errmsg)
    coupy_dll.tprint.argtypes = [c_char_p]
    coupy_dll.tprint.restype = int
    coupy_dll.tprint(errmsg)
    coupy_dll.tprint("\n".encode("utf-8"))
    coupy_dll.tflush.restype = int
    coupy_dll.tflush()
    sys.stdout.flush()
    sys.stderr.flush()
    return RuntimeError({"message": re.sub(" *\0.*", "", errmsg.decode("utf-8"))})

class Coupling():
    """
    Definition of the class Coupling, which establishes a connection between
    functions and classes in C++ to corresponding functions and classes in Python
    """

    def __init__(self, jsonfile):
        """
        Constructor of a Coupling object.
        The object is created using a json-file containing the description of the
        C++ functions and classes.
        """
        self._jsonfile = jsonfile
        with open(self._jsonfile, 'r') as fin:
            self._connection = json.load(fin)
        if 'dll' not in self._connection:
            pass
        elif platform.system() == 'Windows':
            self._connection['dll'] = re.sub('.so$','.dll', self._connection['dll'])
        else:
            self._connection['dll'] = re.sub('.dll$','.so', self._connection['dll'])
        self._generated = []

        self._dirname = os.path.join(os.path.dirname(jsonfile), 'generated')
        if not os.path.isdir(self._dirname):
            os.mkdir(self._dirname)

    def get_generated(self):
        return self._generated

    def set_args_and_restype(self, dll):
        """!
        Set the argtypes and the restype of the (standalone or member)
        functions described in the jsonfile on input
        """
        if 'classes' in self._connection:
            for item in self._connection['classes']:
                Class(item).args_and_restype(dll)
        if 'functions' in self._connection:
            for item in self._connection['functions']:
                Method('c', item).set_args_and_restype(dll)

    def outfilename(self, extension):
        """
        Choose a name for the *.cpp, *.h and *C.py file which will be
        generated by this Couple object
        """
        outfname = re.sub('.json', extension, self._jsonfile)
        return os.path.join(self._dirname, os.path.basename(outfname))

    def write_coupy_cpp(self):
        """
        Create the *.cpp and the *.h file with error handling functions for
        functions and member functions wrapped by C++ouPy.
        """
        outfname = os.path.join(self._dirname, 'C++ouPy.h')
        self._generated.append(outfname)
        with open(outfname, 'w') as fout:
            fout.write('#ifndef CPPOUPY_H\n')
            fout.write('#define CPPOUPY_H\n')
            fout.write('#include <stdio.h>\n')
            fout.write('#include <string.h>\n')
            fout.write('/* Copy the (latest) error message into the output string */\n')
            fout.write('extern "C" void get_error(char string[]);\n')
            fout.write('/* Copy the input string to the (latest) error message */\n')
            fout.write('extern "C" void set_error(const char string[]);\n')
            fout.write('\n#endif\n')

        outfname = os.path.join(self._dirname, 'C++ouPy.cpp')
        self._generated.append(outfname)
        with open(outfname, 'w') as fout:
            fout.write('#include "C++ouPy.h"\n\n')
            fout.write('/* The (latest) error message is stored in a static string */\n')
            fout.write('const size_t n_err_msg=1000;\n')
            fout.write('static char err_msg[n_err_msg];\n\n')
            fout.write('/* Copy one string into the other. Truncate at out_size, and\n')
            fout.write('   make sure the output string ends with a null character */\n')
            fout.write('static char* strncpy_t(char* out, const char* in, size_t out_size){\n')
            fout.write('    for (size_t i =0; i<out_size; i++) {\n')
            fout.write('        out[i] = in[i];\n')
            fout.write('        if (out[i]==(char) 0) break;\n')
            fout.write('    }\n')
            fout.write('    out[out_size-1] = (char)0;\n')
            fout.write('    return out;\n')
            fout.write('}\n\n')
            fout.write('/* Copy the (latest) error message into the output string */\n')
            fout.write('void get_error(char *string) {\n')
            fout.write('    strncpy_t(string, err_msg, strlen(string));\n')
            fout.write(' }\n\n')
            fout.write('/* Copy the input string to the (latest) error message.\n')
            fout.write('   The standard output is flushed because this typically happens\n')
            fout.write('   when C++ code is finished and Python starts again.\n')
            fout.write('   C++ and Python buffer their standard out differently, so it\n')
            fout.write('   comes out in the wrong order if there is no tflush */\n')
            fout.write('void set_error(const char *string) {\n')
            fout.write('    tflush();\n')
            fout.write('    strncpy_t(err_msg, string, n_err_msg);\n')
            fout.write(' }\n')

    def write_cpp(self, hfile=False):
        """
        Create the *.cpp or *.h file which creates C-wrappers for the
        C++ functions and member functions.
        """
        outfname = self.outfilename('.h' if hfile else '.cpp')
        self._generated.append(outfname)
        with open(outfname, 'w') as fout:
            if hfile:
                macro = 'CPPOUPY_'+re.sub(r'\.', '_', os.path.basename(outfname)).upper()
                fout.write('#ifndef '+macro+"\n")
                fout.write('#define '+macro+"\n")
                fout.write('#include <cstddef>\n')
            else:
                if 'hfiles' in self._connection:
                    for item in self._connection['hfiles']:
                        fout.write('#include '+item+'\n')
                for item in ['"'+os.path.basename(self.outfilename('.h'))+'"',
                             '<exception>', '"C++ouPy.h"']:
                    fout.write('#include '+item+'\n')

            if 'classes' in self._connection and \
               'functions' in self._connection:
                fout.write('\n/*************** classes ****************/\n')

            if 'classes' in self._connection:
                for item in self._connection['classes']:
                    Class(item).write_cpp(fout, hfile)

            if 'classes' in self._connection and \
               'functions' in self._connection:
                fout.write('\n/*************** functions ****************/\n')
                fout.write('\n')

            if 'functions' in self._connection:
                for item in self._connection['functions']:
                    Method('c', item).write_cpp(fout, hfile)

            if hfile:
                fout.write('\n#endif\n')

    def write_py(self):
        """
        Write the python-file that loads the DLL containing the C++ functions,
        initialize the calls to the (member)functions, and define some functions
        needed by the generated Python code
        Also, write a python file for each class
        """
        outfname = self.outfilename('C.py')
        main_name = re.sub('.json', 'C', os.path.basename(self._jsonfile))
        self._generated.append(outfname)
        with open(outfname, 'w') as fout:
            fout.write('import re\n')
            fout.write('import os\n')
            fout.write('import sys\n')
            fout.write('import numpy as np\n')
            fout.write('from ctypes import c_long, c_size_t, c_double, c_int # noqa\n')
            fout.write('from coupy.coupling import Coupling, is_ndarray, loaded_dll, ' +
                       'dll_runtime_error #noqa\n')
            if 'import' in self._connection:
                for name in self._connection['import']:
                    fout.write('import '+name)
            if 'dll' in self._connection:
                fout.write("\ndllname = " + self._connection['dll'] + "\n")
            else:
                assert 'dllfun' in self._connection, 'either dll or dllfun must be specified'
                fout.write( self._connection['dllfun'] + '\n')
            fout.write( "dll = loaded_dll(dllname)\n")
             
            fout.write("Coupling(os.path.join(os.path.dirname(__file__),'..','%s'))"
                        % os.path.basename(self._jsonfile) + ".set_args_and_restype(dll)\n\n")
            fout.write("def runtime_error():\n")
            fout.write('    return dll_runtime_error()\n\n')

            if 'functions' in self._connection:
                for item in self._connection['functions']:
                    Method('c', item).write_py(fout)

        autopep8.main(['', '--in-place', outfname])

        if 'classes' in self._connection:
            for item in self._connection['classes']:
                outfname = os.path.join( self._dirname, item['class']+'C.py').replace('\\','/')
                self._generated.append(outfname)
                with open(outfname, 'w') as fout:
                    Class(item).write_py(self._connection, main_name, fout)
                autopep8.main(['', '--in-place', outfname])


coupy_dll = None if not os.path.isfile(dll_name()) else ctypes.cdll.LoadLibrary(dll_name())
