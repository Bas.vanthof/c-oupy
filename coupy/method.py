"""
This module provides the class Method, which represents one method of a class
or a C++-function.
"""
import re
from ctypes import c_void_p
from coupy.argument import Argument, is_basic_c_type


class Method():
    """
    Definition of the class Method, which represents a method or a C++ function.
    """

    def __init__(self, classname, my_dict):
        """
        Constructor of the Method class
        An Method object needs the name of the class ('c' for functions) and
        a dict containing its description
        """
        self._classname = classname
        self._dict = my_dict
        if 'const' not in self._dict:
            self._dict['const'] = True
        else:
            if not isinstance(self._dict['const'],bool):
                msg = "Error in %s::%s, 'const' is a %s but should be a boolean" % \
                      (classname,self._dict['name'],type(self._dict['const']))
                assert isinstance(self._dict['const'],bool), msg

    def myindent(self):
        """
        Enough spaces for the indentation of the code for this Method
        """
        if self._classname != 'c':
            return '    '
        return ''

    def extern_c_name(self):
        """
        The name of the method/function's C-wrapper
        """
        return self._classname+"_"+self._dict['name']

    def extern_c_arguments(self, for_call=True, for_arglist=False):
        """
        Return a list of Argument-objects describing the method/function's arguments
        """
        if self._dict['name'] == 'Create' or \
           self._classname == 'c' or (for_call and not for_arglist):
            args = []
        elif self._dict['const']:
            args = ['const '+self._classname+'* C']
        else:
            args = [self._classname+'* C']

        if 'input' in self._dict:
            args += self._dict['input']
        if not for_call:
            args += ['int *err_code']
        return [Argument(a) for a in args]

    def empty_output(self):
        """
        In case of an error, an empty value should be returned by the C-wrapper.
        This may be a NULL pointer, nothing, or the value zero.
        The correct zero-value is returned when empty_return is set.
        """
        out = self.cpp_output()
        return '' if out == 'void' else \
               '('+out+') ' + ('NULL' if '*' in out else '0')

    def basic_c_output(self):
        if 'output' not in self._dict:
            return False
        return is_basic_c_type(self._dict['output'])

    def cpp_output(self):
        """
        Return a data type of the output value to be used in the C-wrapper.
        """

        if self._dict['name'] == 'Create':
            return self._classname+"*"
        if 'output' not in self._dict:
            return "void"

        out = re.sub(r' *\*.*', '*', self._dict['output']) # keep the * for pointers
        out = re.sub(' .*', '', out)  # keep only first word
        return out

    def output(self):
        """
        Return a data type for the return value in the Python function.
        """
        if self._dict['name'] == 'Create':
            return c_void_p
        if 'output' in self._dict:
            return Argument(self._dict['output']).dtype(out=True)
        return None

    def set_args_and_restype(self, dll):
        """
        Set the data types for the arguments and the return value in the DLL object.
        """
        method_name = self.extern_c_name()
        args = [a.dtype(out=True)
                for a in self.extern_c_arguments(for_arglist=True)] + [c_void_p]
        getattr(dll, method_name).argtypes = args
        if self.output() is not None:
            getattr(dll, method_name).restype = self.output()

    def arglist(self, for_call=False):
        """
        return a list of arguments, for a call to the C++ function (without data types,
        or for the header file/C-Wrapper (with the data-types), depending on the value
        for_call.
        """
        return ', '.join([a.cpptype(for_call)
                          for a in self.extern_c_arguments(for_call)])


    def write_hpp(self, fout):
        """
        Write the function/method to the header file.
        """
        fout.write(self.myindent() + 'extern "C" ')
        out_type = self.cpp_output()
        fout.write(out_type + " " + self.extern_c_name() + "(" + self.arglist() + ")")
        fout.write(';\n')

    def write_cpp_cpp(self, fout, verbose_allocation):
        """
        Write the function/method to the cpp-file.
        The option verbose_allocation causes a message to be printed for every
        (de)allocation, as a diagnostic tool.
        """
        if 'hfile' in self._dict:
            fout.write('#include "'+self._dict['hfile']+'.h"\n')

        out_type = self.cpp_output()
        fout.write(out_type + " " + self.extern_c_name() + "(" + self.arglist() + ")")

        fout.write(" {\n")
        fout.write("     *err_code=1;\n")
        fout.write("     try {\n")
        if 'body' in self._dict:
            for line in self._dict['body']:
                fout.write("        "+line+"\n")
        elif self._dict['name'] == 'Create':
            if self._dict['input']:
                fout.write('        '+self._classname+' *C = new '+
                           self._classname+'('+self.arglist(for_call=True)+');\n')
            else:
                fout.write('        '+self._classname+' *C = new '+self._classname+";\n")
            if verbose_allocation:
                fout.write('        printf("created  %s at %s\\n",(void*) C);\n'
                                    % (self._classname,'%p'))
            fout.write('        tflush();\n')
            fout.write('        return C;\n')
        elif self._classname == 'c':
            if out_type == 'void':
                fout.write('        '+self._dict['name'] +
                          '(' + self.arglist(for_call=True)+');\n')
                fout.write('        tflush();\n')
                fout.write('        return;\n')
            else:
                fout.write('        const '+out_type+' retval = '+self._dict['name'] +
                          '(' + self.arglist(for_call=True)+');\n')
                fout.write('        tflush();\n')
                fout.write('        return retval;\n')
        else:
            if out_type == 'void':
                fout.write('        C->'+self._dict['name'] +
                           '(' + self.arglist(for_call=True)+');\n')
                fout.write('        tflush();\n')
                fout.write('        return;\n')
            else:
                fout.write('        ' + ('const ' if self.basic_c_output() else '') +
                           out_type+' retval = C->'+self._dict['name'] +
                          '(' + self.arglist(for_call=True)+');\n')
                fout.write('        tflush();\n')
                fout.write('        return retval;\n')
        fout.write('     } catch (const std::exception &e) {\n')
        fout.write("        set_error(e.what());\n")
        fout.write("        *err_code=0;\n")
        fout.write("        return "+self.empty_output()+";\n")
        fout.write('     }\n')
        fout.write('}\n')

    def write_cpp(self, fout, hfile, verbose_allocation=False):
        """
        Write the function/method to the cpp-file or to the header file, depending on
        hfile. The option verbose_allocation causes a message to be printed for every
        (de)allocation, as a diagnostic tool.
        """
        if hfile:
            return self.write_hpp(fout)
        return self.write_cpp_cpp(fout, verbose_allocation)

    def header_py(self, fout):
        """
        Write the function/method declaration to the python file
        """
        fout.write('\n\n')
        if self._classname != 'c':
            inargs = ['self']
        else:
            inargs = []

        method_name = self._dict['name'] \
            if self._dict['name'] != 'Create' \
            else '__init__'
        if 'input' in self._dict:
            for arg in [Argument(a) for a in self._dict['input']]:
                if 'const' in arg.description() or \
                   arg.dtype(string=True) == 'object' or \
                   arg.dtype(string=True) == 'c_void_p':
                    inargs.append(arg.name())
        fout.write(self.myindent() + 'def ' + method_name +
                   '(' + ', '.join(inargs)+'):\n')

    def convert_inputs_py(self, fout):
        """
        Write the code that converts/checks the input arguments in the python
        function/method
        """
        if 'input' in self._dict:
            for arg in [Argument(a) for a in self._dict['input']]:
                if arg.python_argument() is not None:
                    fout.write(self.myindent() + '    '+arg.python_argument()+'\n')

    def c_call_py(self, fout):
        """
        write the call to the C-wrapper in the python fuction/method
        """
        if self._classname == 'c' or self._dict['name'] == 'Create':
            allargs = []
        else:
            allargs = ['self._C']

        if 'input' in self._dict:
            allargs += [Argument(a).python_argument(for_call=True)
                        for a in self._dict['input']]

        allargs += [Argument('int *err_code').python_argument(for_call=True)]

        if 'output' in self._dict:
            out = Argument(self._dict['output']).name() + ' = '
        elif self._dict['name'] == 'Create':
            out = 'self._C = '
        else:
            out = ''

        fout.write(self.myindent() + '    err_code = np.zeros((1),dtype=c_int)\n')
        fout.write(self.myindent() + '    '+out+'dll.' +
                   self.extern_c_name() + '(' + ', '.join(allargs)+')\n')
        fout.write(self.myindent() + '    if err_code[0] != 1:\n')
        fout.write(self.myindent() + '        raise(runtime_error())\n')
        if self._dict['name'] == 'Create':
            fout.write(self.myindent() + '    self._Cmine=True\n')



    def return_py(self, fout):
        """
        Write the return statement for the python file
        """

        if self._dict['name'] == 'Create':
            return

        outargs = [Argument(self._dict['output']).name()
                   ] if 'output' in self._dict else []
        if 'input' in self._dict:
            outargs += [Argument(a).python_argument(for_return=True)
                        for a in self._dict['input']]
        outargs = [a for a in outargs if a]
        fout.write(self.myindent() + '    return '+', '.join(outargs)+'\n')

    def write_py(self, fout):
        """
        Write the method or function to the Python-file
        """
        self.header_py(fout)
        self.convert_inputs_py(fout)
        self.c_call_py(fout)
        self.return_py(fout)
