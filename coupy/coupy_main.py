#!/usr/bin/env python3
"""
Main script for C++ouPy, a system that makes C++ functions and classes available to Python
"""
import argparse
import os
import sys

import coupy.argument
from coupy.coupling import Coupling, dll_name, include_name, dll_dir

sys.path.insert(1, os.path.dirname(__file__))

def show_doc():
    os.system("evince " + os.path.join(os.path.dirname(__file__),'doc','C++ouPy_manual.pdf') + '&' )
    quit()

class Formatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawTextHelpFormatter):
    """
    This class combines some available features from argparse classes
    """


class Parser(argparse.ArgumentParser):
    """
    Configure the command line options.
    """
    def __init__(self, description):
        argparse.ArgumentParser.__init__(self, description=description, formatter_class=Formatter)

    def add_bool(self, name, default=True, help=None):
        self.add_argument("--"+name, action=argparse.BooleanOptionalAction, help=help, default=default)

def usage():
    """
    Set up the command line interface
    """
    parser = Parser(main.__doc__)
    parser.add_bool("doc", help="open the user manual (in pdf-viewer), then quit", default=False)
    parser.add_bool("include", help="print the name of the include directory, then quit", default=False)
    parser.add_bool("dlldir", help="print the directory name of the dll, then quit", default=False)
    parser.add_bool("dll", help="print the name of the dll, then quit", default=False)
    parser.add_bool("generated-files", help="print the names of the generated cpp files " + 
                    "after generating them for further processnig in CMake",
                    default=False)
    parser.add_bool("test", help="run the doctests of C++ouPy", default=False)
    parser.add_argument('jsonfile', nargs=argparse.REMAINDER, help = "(json) files to process")

    args = parser.parse_args()
    if args.doc:
        show_doc()
    return args


def generate_couplings(jsonfiles, generated_files):
    files = []
    for jsonfile in jsonfiles:
        if '.json' in jsonfile:
            if not generated_files:
                print("Coupling "+jsonfile)
            coupling = Coupling(jsonfile)
            #coupling.write_coupy_cpp()
            coupling.write_cpp(hfile=True)
            coupling.write_cpp(hfile=False)
            coupling.write_py()
            files += coupling.get_generated() 
    return files

def main():
    """
    Create code to couple C++ to Python software
    """
    args = usage()
    if args.include:
        print(include_name())
        quit()
    if args.dlldir:
        print(dll_dir())
        quit()
    if args.dll:
        print(dll_name())
        quit()
    if args.test:
        import doctest
        nfail, ntest = doctest.testmod(m=coupy.argument)
        if nfail>0:
            raise ValueError('%d out of %d tests failed' % (nfail,ntest))
        print("All ",ntest,' tests were successful')

    files = generate_couplings(args.jsonfile, args.generated_files)

    if  args.generated_files:
        print(';'.join([fname.replace('\\','/') for fname in files if '.cpp' in fname]))
    else:
        print("\nThe following files were generated in this run:\n    " + 
              '\n    '.join(files) + '\n')

if __name__ == "__main__":
    main()

