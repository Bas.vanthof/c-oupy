#!/usr/bin/env python3
"""
This module provides the class Argument, which represents one argument
in a call to a C++ function or method.
"""
import re
import doctest
from ctypes import c_void_p, c_int, c_double, c_char_p, c_long, c_size_t, c_bool

def is_basic_c_type(descr):
    """
    returns True if the description is a basic C type, that is, if it contains
    'int', 'double', 'bool', 'long' or 'size_t' as a whole word, but not the last one

    >>> is_basic_c_type('char int')
    False

    >>> is_basic_c_type('int yolo')
    True
    """
    return bool(re.search(r'\b(bool|double|int|long|size_t)\b.*[a-zA-Z]',descr))


class Argument():
    """!
    definition of class Argument
    An Argument object describes one argument of a C++ function or method.
    """

    def __init__(self, descr):
        """!
        Constructor of an argument.

        The constructor takes the description, which is how it is called in the
        C++-function or method, e.g. Argument('int ix')
        """
        self._descr = descr

    def description(self):
        """!
        return the description as given to __init__
        """
        return self._descr

    def dtype_string(self):
        """!
        Return self._descr simple indication from ctypes:
        c_char_p, c_void_p, c_int, c_long, c_size_t or c_double.

        >>> Argument('const int number').dtype_string()
        'c_int'

        >>> Argument('int * number').dtype_string()
        'c_int'

        >>> Argument('int *number').dtype_string()
        'c_int'

        >>> Argument('const Class * my_obj').dtype_string()
        'object'

        """

        translation = {   'bool':'c_bool',
                        'double':'c_double',
                        'size_t':'c_size_t',
                          'long':'c_long',
                           'int':'c_int',
                          'char':'c_char_p',
                         'void*':'c_void_p'}

        for key in translation:
            if re.search(r'\b\**'+key+r'\**\b', self._descr):
                return translation[key]
        return 'object'


    def dtype(self, out=False, string=False):
        """!
        Return self._descr simple indication from ctypes:
        c_char_p, c_void_p, c_int, c_long, c_size_t or c_double.
        'const' must be in the argument, unless <out> is True.

        >>> Argument('const int number').dtype()
        <class 'ctypes.c_int'>

        >>> Argument('const double number').dtype()
        <class 'ctypes.c_double'>

        >>> Argument('const long number').dtype()
        <class 'ctypes.c_long'>

        >>> Argument('const size_t number').dtype()
        <class 'ctypes.c_ulong'>

        >>> Argument('const char * string').dtype()
        <class 'ctypes.c_char_p'>

        >>> Argument('double number').dtype(out=True)
        <class 'ctypes.c_double'>

        >>> Argument('long number').dtype(out=True)
        <class 'ctypes.c_long'>

        >>> Argument('size_t number').dtype(out=True)
        <class 'ctypes.c_ulong'>

        >>> Argument('double number').dtype()
        Traceback (most recent call last):
        ...
        ValueError: argument "double number" is not a constant

        >>> Argument('long number').dtype()
        Traceback (most recent call last):
        ...
        ValueError: argument "long number" is not a constant

        >>> Argument('size_t number').dtype()
        Traceback (most recent call last):
        ...
        ValueError: argument "size_t number" is not a constant

        >>> Argument('Model object').dtype()
        Traceback (most recent call last):
        ...
        ValueError: unrecognized argument description "Model object"
        """

        if string:
            return self.dtype_string()

        if 'char ' not in self._descr and '*' not in self._descr and \
           not is_basic_c_type(self._descr):
            raise ValueError(
                'unrecognized argument description "'+self._descr+'"')

        if not out and 'const' not in self._descr:
            raise ValueError('argument "'+self._descr+'" is not a constant')


        if 'char *' in self._descr:
            return c_char_p
        if '[' in self._descr or '*' in self._descr:
            return c_void_p

        translation = {   'bool':c_bool,
                        'double':c_double,
                        'size_t':c_size_t,
                          'long':c_long,
                           'int':c_int}

        for key in translation:
            if re.search(r'\b\**'+key+r'\**\b', self._descr):
                return translation[key]
        return None

    def name(self):
        """!
        Return the name of an argument

        >>> Argument('const int number').name()
        'number'

        >>> Argument('int * number').name()
        'number'

        >>> Argument('int *number').name()
        'number'

        >>> Argument('const Class * my_obj').name()
        'my_obj'

        >>> Argument('const double number').name()
        'number'

        >>> Argument('const long number').name()
        'number'

        >>> Argument('const size_t number').name()
        'number'

        >>> Argument('const char * string').name()
        'string'

        """
        return re.sub(r'\[.*', '', re.sub(r'.* \**', '', self._descr))

    def cpptype(self, for_call=False):
        """!
        Return an argument for use in a declaration (for_call=False)
        or for use in a function call (for_call=True)

        In a declaration with compile-time (all-cap) dimensions
          void my_function(..., const double array[N3D], ...) { ... }
        >>> Argument('const double array[N3D]').cpptype()
        'const double array[N3D]'

        >>> Argument('const long array[N3D]').cpptype()
        'const long array[N3D]'

        >>> Argument('const size_t array[N3D]').cpptype()
        'const size_t array[N3D]'

        In a declaration with run-time (not all-cap) dimensions
          void my_function(..., const double array[self.ndims()], ...) { ... }
        >>> Argument('const double array[self.ndims()]').cpptype()
        'const double array[]'

        >>> Argument('const long array[self.ndims()]').cpptype()
        'const long array[]'

        >>> Argument('const size_t array[self.ndims()]').cpptype()
        'const size_t array[]'

        In a function call
          my_function(..., array, ...) { ... }
        >>> Argument('const double array[N3D]').cpptype(for_call=True)
        'array'

        >>> Argument('const long array[N3D]').cpptype(for_call=True)
        'array'

        >>> Argument('const size_t array[N3D]').cpptype(for_call=True)
        'array'

        You can use spaces to manipulate how a call will be passed.
        To specify an argument that is received as a pointer (*outvalue)
        by the function, use a space:
        >>> Argument('double * outvalue').cpptype(for_call=True)
        'outvalue'

        >>> Argument('long * outvalue').cpptype(for_call=True)
        'outvalue'

        >>> Argument('size_t * outvalue').cpptype(for_call=True)
        'outvalue'

        To specify an argument that is received as a reference (&outvalue)
        by the function, use no space:
        >>> Argument('double *outvalue').cpptype(for_call=True)
        '*outvalue'

        >>> Argument('long *outvalue').cpptype(for_call=True)
        '*outvalue'

        >>> Argument('size_t *outvalue').cpptype(for_call=True)
        '*outvalue'

        """
        if for_call:
            return re.sub(r'\[.*', '', re.sub('.* ', '', self._descr))
        if re.search(r'\[.*[a-z].*\]', self._descr):
            return re.sub(r'\[.*[a-z].*\]', '[]', self._descr)
        return self._descr

    def python_argument_for_call(self):
        """!

        Scalar strings (input or output) are simply the variable name:
        >>> Argument("const char * string").python_argument(for_call=True)
        'string'

        >>> Argument("char * string").python_argument_for_call()
        'string'

        Scalar input arrays: also just the name
        >>> Argument("double outval").python_argument_for_call()
        'outval'

        >>> Argument("long outval").python_argument_for_call()
        'outval'

        >>> Argument("size_t outval").python_argument_for_call()
        'outval'

        For scalar output arguments, the data-pointer
        >>> Argument("double * outval").python_argument_for_call()
        'outval.ctypes.data'

        >>> Argument("long * outval").python_argument_for_call()
        'outval.ctypes.data'

        >>> Argument("size_t * outval").python_argument_for_call()
        'outval.ctypes.data'

        For arrays, the data-pointer is used:
        >>> Argument("const double inarr[self.ndim()]").python_argument_for_call()
        'inarr.ctypes.data'

        >>> Argument("const long inarr[self.ndim()]").python_argument_for_call()
        'inarr.ctypes.data'

        >>> Argument("const size_t inarr[self.ndim()]").python_argument_for_call()
        'inarr.ctypes.data'

        For objects, the C++-pointer is used:
        >>> Argument("Object * my_obj").python_argument_for_call()
        'my_obj._C'

        Write the code needed to convert the argument for a python
        function.
        """

        return self.name() + \
              ('._C'          if self.dtype(string=True) == 'object' else
               '.ctypes.data' if 'char ' not in self._descr and \
                                  ('*' in self._descr or '[' in self._descr) else '')

    def python_argument_for_return(self):
        """!

        **** For the return statement *****
        Input strings are not returned
        >>> Argument("const char * string").python_argument_for_return()

        Output strings are converted
        >>> Argument("char * string").python_argument_for_return()
        "re.sub(' *\\\\0.*', '', string.decode('utf-8'))"

        Output strings are converted
        >>> Argument("char string[nlen]").python_argument_for_return()
        "re.sub(' *\\\\0.*', '', string.decode('utf-8'))"

        For scalar output arguments, the first item
        >>> Argument("double * outval").python_argument_for_return()
        'outval[0]'

        >>> Argument("long * outval").python_argument_for_return()
        'outval[0]'

        >>> Argument("size_t * outval").python_argument_for_return()
        'outval[0]'

        Input arrays are never returned
        >>> Argument("const double inarr[self.ndim()]").python_argument_for_return() # noqa

        >>> Argument("const long inarr[self.ndim()]").python_argument_for_return() # noqa

        >>> Argument("const size_t inarr[self.ndim()]").python_argument_for_return() # noqa

        Output arrays are returned
        >>> Argument("double inarr[self.ndim()]").python_argument_for_return()
        'inarr'

        >>> Argument("long inarr[self.ndim()]").python_argument_for_return()
        'inarr'

        >>> Argument("size_t inarr[self.ndim()]").python_argument_for_return()
        'inarr'

        Objects are never returned:
        >>> Argument("Object * my_obj").python_argument_for_return()

        """

        if 'const' in self._descr or self.dtype(string=True) == 'object':
            return None
        if 'char ' in self._descr and ('*' in self._descr or '[' in self._descr):
            return "re.sub(' *\\0.*', '', "+self.name()+".decode('utf-8'))"
        if is_basic_c_type(self._descr) and \
              'const' not in self._descr and '[' not in self._descr:
            return self.name()+'[0]'
        return self.name()


    def python_argument_allocate(self):
        """!

        **** Allocation ****
        Output variables are to be initialized:
        Strings are initialized to a zero-terminated byte-string:
        >>> Argument("char * string").python_argument_allocate()
        'string = (" "*20 + "\\\\0").encode("utf-8")'

        >>> Argument("char string[100]").python_argument_allocate()
        'string = (" "*100 + "\\\\0").encode("utf-8")'

        Scalar output data is initialized to numpy-arrays:
        >>> Argument("int * outvalue").python_argument_allocate()
        'outvalue = np.zeros((1), dtype=c_int)'

        Numerical data is initialized to numpy-arrays:
        >>> Argument("double outarr[N3D,7]").python_argument_allocate()
        'outarr = np.zeros((N3D,7), dtype=c_double)'

        >>> Argument("long outarr[N3D,7]").python_argument_allocate()
        'outarr = np.zeros((N3D,7), dtype=c_long)'

        >>> Argument("size_t outarr[N3D,7]").python_argument_allocate()
        'outarr = np.zeros((N3D,7), dtype=c_size_t)'

        """

        selfd = self.description()

        if 'char ' in selfd and ('*' in selfd  or '[' in selfd):
            strlen = '20'
            if re.search(r'\[.*[a-z0-9].*\]', selfd):
                strlen = re.sub(r'.*\[', '', re.sub(r'\].*', '', selfd))
            return self.name() + r' = (" "*%s + "\0").encode("utf-8")' % strlen

        if is_basic_c_type(selfd) and 'const' not in selfd:
            if '[' in selfd:
                outdims = re.sub(r'.*\[', '', re.sub(r'\].*', '', selfd))
            else:
                outdims = '1'
            return  self.name() + ' = np.zeros(('+outdims+')' + ', dtype=' + \
                self.dtype(string=True)+')'

        return None

    def python_argument_assert(self):
        """!

        **** Check ****
        If they are not converted, input variables are to be asserted:
        Strings must be zero-terminated byte-strings:
        >>> Argument("const char * string").python_argument_assert()
        'assert is_c_string(string), "string should be a zero-terminated byte-string"'

        Numerical data must be numpy-arrays:
        >>> Argument("const double inarr[self.ndim()]").python_argument_assert()
        'assert is_ndarray(inarr,c_double), "inarr should be a numpy-array with data type c_double"'

        >>> Argument("const long inarr[self.ndim()]").python_argument_assert()
        'assert is_ndarray(inarr,c_long), "inarr should be a numpy-array with data type c_long"'

        >>> Argument("const size_t inarr[self.ndim()]").python_argument_assert()
        'assert is_ndarray(inarr,c_size_t), "inarr should be a numpy-array with data type c_size_t"'

        Anything else is not converted or checked
        >>> Argument("const int ix").python_argument_convert()

        >>> Argument("Object myobj").python_argument_convert()

        """


        selfd = self.description()
        if 'char ' in selfd and '*' in selfd:
            return ('assert is_c_string(%s), ' +
                    '"%s should be a zero-terminated byte-string"') % \
                   (self.name(), self.name())
        if '[' in selfd or '*' in selfd:
            return ('assert is_ndarray(%s,%s), ' +
                    '"%s should be a numpy-array with data type %s"') % \
                   (self.name(), self.dtype(string=True), \
                    self.name(), self.dtype(string=True))
        return None

    def python_argument_convert(self):
        """!

        **** Conversion ****
        Input variables are to be converted:
        Strings are converted to zero-terminated byte-strings:
        >>> Argument("const char * string").python_argument_convert()
        'string = (string + "\\\\0").encode("utf-8")'

        Numerical data is converted to numpy-arrays:
        >>> Argument("const double inarr[self.ndim()]").python_argument_convert()
        'inarr = np.array(inarr, dtype=c_double)'

        >>> Argument("const long inarr[self.ndim()]").python_argument_convert()
        'inarr = np.array(inarr, dtype=c_long)'

        >>> Argument("const size_t inarr[self.ndim()]").python_argument_convert()
        'inarr = np.array(inarr, dtype=c_size_t)'

        Anything else is not converted
        >>> Argument("const int ix").python_argument_convert()

        >>> Argument("Object myobj").python_argument_convert()

        """


        selfd = self.description()
        if 'char ' in selfd and '*' in selfd:
            return self.name() + ' = ('+self.name()+' + "\\0").encode("utf-8")'
        if '[' in selfd or '*' in selfd:
            return self.name() + ' = np.array('+self.name() + ', dtype=' + \
                self.dtype(string=True)+')'
        return None

    def python_argument(self, for_call=False, for_return=False):
        """!

        **** For the return statement *****
        Input strings are not returned
        >>> Argument("const char * string").python_argument(for_return=True)

        Output strings are converted
        >>> Argument("char * string").python_argument(for_return=True)
        "re.sub(' *\\\\0.*', '', string.decode('utf-8'))"

        Output strings are converted
        >>> Argument("char string[nlen]").python_argument(for_return=True)
        "re.sub(' *\\\\0.*', '', string.decode('utf-8'))"

        For scalar output arguments, the first item
        >>> Argument("double * outval").python_argument(for_return=True)
        'outval[0]'

        >>> Argument("long * outval").python_argument(for_return=True)
        'outval[0]'

        >>> Argument("size_t * outval").python_argument(for_return=True)
        'outval[0]'

        Input arrays are never returned
        >>> Argument("const double inarr[self.ndim()]").python_argument(for_return=True) # noqa

        >>> Argument("const long inarr[self.ndim()]").python_argument(for_return=True) # noqa

        >>> Argument("const size_t inarr[self.ndim()]").python_argument(for_return=True) # noqa

        Output arrays are returned
        >>> Argument("double inarr[self.ndim()]").python_argument(for_return=True)
        'inarr'

        >>> Argument("long inarr[self.ndim()]").python_argument(for_return=True)
        'inarr'

        >>> Argument("size_t inarr[self.ndim()]").python_argument(for_return=True)
        'inarr'

        Objects are never returned:
        >>> Argument("Object * my_obj").python_argument(for_return=True)

        **** For a call ****
        Scalar strings (input or output) are simply the variable name:
        >>> Argument("const char * string").python_argument(for_call=True)
        'string'

        >>> Argument("char * string").python_argument(for_call=True)
        'string'

        Scalar input arrays: also just the name
        >>> Argument("double outval").python_argument(for_call=True)
        'outval'

        >>> Argument("long outval").python_argument(for_call=True)
        'outval'

        >>> Argument("size_t outval").python_argument(for_call=True)
        'outval'

        For scalar output arguments, the data-pointer
        >>> Argument("double * outval").python_argument(for_call=True)
        'outval.ctypes.data'

        >>> Argument("long * outval").python_argument(for_call=True)
        'outval.ctypes.data'

        >>> Argument("size_t * outval").python_argument(for_call=True)
        'outval.ctypes.data'

        For arrays, the data-pointer is used:
        >>> Argument("const double inarr[self.ndim()]").python_argument(for_call=True)
        'inarr.ctypes.data'

        >>> Argument("const long inarr[self.ndim()]").python_argument(for_call=True)
        'inarr.ctypes.data'

        >>> Argument("const size_t inarr[self.ndim()]").python_argument(for_call=True)
        'inarr.ctypes.data'

        For objects, the C++-pointer is used:
        >>> Argument("Object * my_obj").python_argument(for_call=True)
        'my_obj._C'

        Write the code needed to convert the argument for a python
        function.

        **** Conversion ****
        Input strings are converted to zero-terminated byte-strings:
        >>> Argument("const char * string").python_argument()
        'string = (string + "\\\\0").encode("utf-8")'

        **** Assertion ****
        Numerical data must be numpy-arrays:
        >>> Argument("const double inarr[self.ndim()]").python_argument()
        'assert is_ndarray(inarr,c_double), "inarr should be a numpy-array with data type c_double"'

        >>> Argument("const long inarr[self.ndim()]").python_argument()
        'assert is_ndarray(inarr,c_long), "inarr should be a numpy-array with data type c_long"'

        **** Allocation ****
        Output variables are to be initialized:
        Strings are initialized to a zero-terminated byte-string:
        >>> Argument("char * string").python_argument()
        'string = (" "*20 + "\\\\0").encode("utf-8")'

        >>> Argument("char string[100]").python_argument()
        'string = (" "*100 + "\\\\0").encode("utf-8")'

        Scalar output data is initialized to numpy-arrays:
        >>> Argument("int * outvalue").python_argument()
        'outvalue = np.zeros((1), dtype=c_int)'

        Numerical data is initialized to numpy-arrays:
        >>> Argument("double outarr[N3D,7]").python_argument()
        'outarr = np.zeros((N3D,7), dtype=c_double)'

        >>> Argument("long outarr[N3D,7]").python_argument()
        'outarr = np.zeros((N3D,7), dtype=c_long)'

        >>> Argument("size_t outarr[N3D,7]").python_argument()
        'outarr = np.zeros((N3D,7), dtype=c_size_t)'

        Anything else is not converted or initialized
        >>> Argument("const int ix").python_argument()

        >>> Argument("Object myobj").python_argument()

        """

        if for_return:
            return self.python_argument_for_return()

        if for_call:
            return self.python_argument_for_call()

        if self.dtype(string=True) == 'object':
            return None

        selfd = self.description()
        if 'const' not in selfd:
            return self.python_argument_allocate()
        if 'char' in selfd:
            return self.python_argument_convert()
        return self.python_argument_assert()

if __name__ == "__main__":
    doctest.testmod()
