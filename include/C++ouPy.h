#ifndef CPPOUPY_H
#define CPPOUPY_H
#include <stdio.h>
#include <string.h>
/* Copy the (latest) error message into the output string */
extern "C" void get_error(char string[]);

/* Copy the input string to the (latest) error message */
extern "C" void set_error(const char string[]);

/* Open a logfile: tprintf will print to stdout AND to the logfile.*/
extern "C" int open_logfile(const char fname[]);

/* Close the logfile: tprintf will print only to stdout from now on */
extern "C" int close_logfile();

/* Flush stdout, stderr, logfile */
extern "C" int tflush();

/* Print to stdout, and also to logfile if there is one */
extern "C" int tprintf(const char *fmt, ...);

/* Print to stdout, and also to logfile if there is one */
extern "C" int tprint(const char *string);

#endif
