\documentclass{article}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage[margin=2cm]{geometry}
\usepackage{float}
\usepackage{fancyhdr}
\pagestyle{fancy}
\definecolor{darkblue}{rgb}{0.0, 0.0, 0.6}
\definecolor{darkgreen}{rgb}{0.0, 0.4, 0.26}
\newcommand{\cppUitleg}[1]{{\color{darkblue} #1\\[1ex]}}
\renewcommand{\cppUitleg}[1]{}
\fancyhf{}
\fancyhead[LE,LO]{
      \hspace{-0.1cm} \begin{tabular}{c} \\[-1ex] \includegraphics[width=30mm]{Logo/C++ouPy-Logo.pdf}
           \\{\Large C++ouPy} \end{tabular}
}
\fancyfoot[LE,RO]{C++ouPy manual, page \thepage}
\setlength{\headheight}{3.5cm}
\setlength{\textheight}{21cm}

\renewcommand{\familydefault}{\sfdefault}
\title{C++ouPy, a coupling of C++ functions and classes to Python\\
User manual}
\author{Bas van 't Hof, for Leiden university}
\date{Dec 14, 2020}


\usepackage{listings}
\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  commentstyle=\color{darkgreen},    % comment style
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  showstringspaces=false,
}

\begin{document}
\maketitle
\tableofcontents
\clearpage
\section{Coupling C++ code to Python}
Python is a very powerful programming language, that allows you, among other things, to visualize 
data in many attractive ways (using {\tt matplotlib}), and to use a lot of available, highly advanced,
numerical methods (from {\tt scipy}).  Many numerical calculations can be supported in a very efficient
way, using {\tt numpy}.

Many numerical calculations, however, cannot be supported efficiently by {\tt numpy}: when inner loops have to
be programmed, Python is relatively inefficient and even {\tt numba} cannot always make up for that.

In such cases, the inner loops have to be programmed in a language which is closer to the computer code, 
which can be compiled, and which can use all the possibilities in {\tt omp} and/or {\tt mpi} and which can 
optimize vectorization and cache-use.

The resulting code will have a main program, a user-interface, and visualization in Python, but it will have 
its heaviest model-calculations in another language. The other language will, for instance, evaluate the 
evolution function and/or its linearization.
Time-integration methods, optimizations, matrix manipulation etcetera will be done in Python.

C++ is a good candidate for the inner-loops, because it allows very low-level optimization like C, but also 
supports object orientation in a way very similar to Python.

C++ouPy (pronounce {\em choopey}) couples C++ code to Python in such a way, that each hybrid class may consist of 
both Python and C++ code, so every aspect of each class is supported in the most suitable language.
It does not simply take any C++ code to make it available to Python: certain assumptions are made about 
how the C++ classes are set up. The result is that only very little generated code is needed, and that the 
two languages connect almost seamlessly in hybrid classes.

\clearpage
\section{Hello, world!}
Of course we start our tour of C++ouPy by implementing 'Hello, World!'. We need the C++-code, 
shown in Table \ref{Tab: hello, world c++}.
\begin{table}[H]
\begin{tabular}{|l|l|}
\hline
header file {\tt hello\_world.h} &  source file {\tt hello\_world.cpp}\\ \hline
\begin{minipage}{8cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#ifndef CPPOUPY_HELLOWORLD_H
#define CPPOUPY_HELLOWORLD_H
void hello_world(void);
#endif
\end{lstlisting}
\end{minipage} & 
\begin{minipage}{8cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#include "hello_world.h"
#include <cstdio>
void hello_world(void) {
   printf("\n      Hello, world!\n\n");
}
\end{lstlisting}
\end{minipage} 
\\ \hline
\end{tabular}
\caption{\em C++ implementation of 'Hello, World!'}
\label{Tab: hello, world c++}
\end{table}

\cppUitleg{
\subsection{Header and source files}
C++ code (like C code) typically comes in pairs of files: a {\em header file} {\tt *.h} and a {\em source file} {\tt *.cpp} ({\tt *.c} in C).
The header file contains declarations of the elements in the code that the source file will make available. In this case, the header file
announces that a function {\tt hello\_world} will be defined in the source file.

The source file {\em includes} its own header file and also all header files of the elements that will be used.  
Elements can be functions, classes, constants, macros, etcetera.  
This will make it possible for the compiler to check that all those elements are used correctly.

\subsection{Preprocessing}
C++ code, like C code, is passed through the {\em preprocessor} {\tt cpp} before it is given to the compiler.
Most (but not all) lines that start with a pound sign ({\tt \#}) are meant for the preprocesor. In the 
{\tt hello\_world} example, we see {\tt \#include}, {\tt \#ifdef}, {\tt \#define} and {\tt \#endif}.

The purpose of {\tt \#include} was mentioned above. Apart from the inclusion of the header file {\tt hello\_world.h}, 
we see also the inclusion of {\tt <cstdio>}.  Note the difference in the include-syntax: {\tt hello\_world.h} is in 
double quotes, and {\tt cstdio} is in {\em angle brackets}.  The reason is that {\tt cstdio} is the header file for a standard 
C++ library.  Apparently, it is not so much part of the language that you have to announce it when you want to use it.
This is similar to python, where {\tt numpy}, {\tt scipy}, {\tt matplotlib}, {\tt sys}, {\tt os}, {\tt re}, {\tt scipy} etcetera
are really thought of as part of the language, but they still have to be {\tt import}ed.

The {\tt \#ifdef} and {\tt \#define} construction in {\tt hello\_world.h} is a standard practice for header files.
The preprocessor commands {\tt \#define} and {\tt \#ifdef} are used often in C++ and for all kinds of purposes, but 
the given example is a very specific case. The compiler gets confused when the same elements are announced multiple times.
However, when including multiple header files, it is quite possible that one of the header files will already include 
another header file. This happens for instance when the header file declares functions on classes defined in another 
header file. The {\tt \#ifdef}-construction causes every subsequent {\tt \#include} to have no effect after a
header file is included once. The reader can probably figure out how it works.
}

The C++ code is compiled (using Cmake) into the dll {\tt build/libC++ouPy.so}.
Next, we need the coupling specification file {\tt cHelloWorld.json}, shown in Table \ref{Tab: hello, world json}.
\begin{table}[H]
\begin{lstlisting}[language=Python,basicstyle=\ttfamily,columns=fullflexible]
{
   "dll":       "../build/libC++ouPy.so",
   "hfiles":    ["\"hello_world.h\""],
   "functions": [ { "name":   "hello_world" } ]
}
\end{lstlisting}
\caption{\em Coupling file {\tt tests/cHelloWorld.json}}
\label{Tab: hello, world json}
\end{table}
The coupling is established by running {\tt C++ouPy.py}:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,columns=fullflexible]
C++ouPy.py ./tests/HelloWorld.json
\end{lstlisting}
The following files are generated in the folder {\tt tests/generated} by C++ouPy:\\[1ex]
\begin{tabular}{ll}
{\tt HelloWorld.h}, {\tt HelloWorld.cpp} & C-wrappers for the C++ code\\
{\tt HelloWorldC.py} & The python-script containing the function {\tt hello\_world}.\\
\end{tabular}\\[1ex]
The generated code is so short that it can be included integrally in this manual.

\subsection{Error handling functions}
The error handling functions are not generated: they are part of {\tt C++ouPy}. 
The header-file is shown in Table \ref{Tab: error handling}: the source file simply implements 
copying strings between arguments and a static {\tt char}-array.
\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
header file {\tt C++ouPy.h}\\ \hline
\begin{minipage}{17cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#ifndef CPPOUPY_H
#define CPPOUPY_H
#include <stdio.h>
#include <string.h>

/* Copy the (latest) error message into the output string */
extern "C" void get_error(char string[]);

/* Copy the input string to the (latest) error message */
extern "C" void set_error(const char string[]);
#endif
\end{lstlisting}
\end{minipage} \\ \hline
\end{tabular}
\caption{\em C++ouPy's error handling functions}
\label{Tab: error handling}
\end{table}
There is sufficient explanation in the source file about how these functions work, 
so they will not be explained in the text of this manual again.

\cppUitleg{
\subsection{Data types}
The header file {\tt C++ouPy.h} does not have a lot in it that we haven't seen in 
Table \ref{Tab: hello, world c++}.  The only new feature in this file comes from the 
use of the function argument {\tt string} in both declared functions.
Note that the argument is declared differently in the two functions: in {\tt get\_error}, it 
{\tt char *string}, and in {\tt set\_error}, it is {\tt const char *string}.
This is because the string is altered in {\tt get\_error} but not in {\tt set\_error}.
The compiler actually checks that the function specified in the source file indeed 
leaves the arguments marked {\tt const} unchanged.

The word {\tt char} in the declaration cannot be surprising: if we are dealing with strings, 
characters must be involved. However, what is the star doing in this declaration? It is not 
just decoration, but it means that the argument is {\em not} a character, but a {\em pointer}
to a character: the memory address where the character is found.

Pointers are used in C++ for many purposes. In this case, the pointer is used because this is 
the most primitive way in which an array (a concept very similar to a {\tt list} in Python) is 
represented. If you know where in memory an item is found (a character in this case), and you know
how many bytes each item occupies (one, in this case), you can find the next one easily. And the 
one after that. The notation {\tt string[i]} is used to indicate the {\tt i}-th item in the array {\tt string}.

Note that the pointer has no information about {\em how many} items there are. 
If the array has 12 items, the source code can access (or change) the 13th item, 
causing memory corruption. The operator system is clever enough to make sure the whole computer is not 
messed up when this happens, but your code is likely to produce nonsense, and sometimes the program will
be terminated by the operator system: this is called a {\em crash}.

The source file shows, besides {\tt char} pointers, also variables of the types {\tt size\_t}, 
a type that can represent very large nonnegative integers.

\subsection{Static variables and functions}
The source file contains the word {\tt static} twice: for the declaration of the {\tt err\_msg} and
for the function {\tt strncpy\_t}.  

Unlike 'normal' variables, {\tt static} a variable keeps its value, 
even when the program is processing lines from other source files.
The {\tt static} variable {\tt err\_msg} can be accessed from anywhere in the source file 
in which it is declared, because its declaration is outside all 
{\em scopes} (text area between curly brackets {\tt \{..\}}). Static variables cannot be accessed from outside the
source file.

Similarly, a {\tt static} function can be called from anywhere in the source file in which it is declared 
and not from outside.

\subsection{Function declarations}
In Table \ref{Tab: hello, world c++}, it may have seemed that function declarations always start with the word {\tt void}:
{\tt void} is C++ for what Python calls {\tt def}. This is not the case. A function is declared like a variable, 
indicating the data type of the function's return value.  For functions without a return value, we need a word for 'nothing',
and that is {\tt void}.
}

\subsection{Wrapper code}
After the error handling files {\tt C++ouPy.h} and {\tt C++ouPy.cpp}, we see the {\em wrapper files} 
{\tt HelloWorld.h} and {\tt HelloWorld.cpp}. They are shown in Table \ref{Tab: hello world wrappers}.
\begin{table}[H]
\begin{tabular}{|l|l|}
\hline
header file {\tt HelloWorld.h} & source file {\tt hello\_world.cpp}\\ \hline
\begin{minipage}{8cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#ifndef HELLOWORLD_H
#define HELLOWORLD_H
extern "C" void c_hello_world(int *err_code);
#endif
\end{lstlisting}
\end{minipage} &
\begin{minipage}{8cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#include "HelloWorld.h"
#include <exception>
#include "C++ouPy.h"
#include "hello_world.h"
void c_hello_world(int *err_code) {
     *err_code=1;
     try {
        hello_world();
        fflush(stdout);
        return;
     } catch (const std::exception &e) {
        set_error(e.what());
        *err_code=0;
        return ;
     }
}
\end{lstlisting}
\end{minipage} 
\\ \hline
\end{tabular}
\caption{\em Wrapper files {\tt HelloWorld.h} and {\tt HelloWorld.cpp}}
\label{Tab: hello world wrappers}
\end{table}

Python connects very easily to C-functions in a DLL. C++ functions are harder, 
because C++ allows multiple functions of the same name if they can be distinguished,
for instance if they are methods (member functions) of a class, or if they have different
argument declarations.

C++ouPy makes the connection to the C++ functions and methods by {\em wrapping} them in 
a C-function. This is what the term {\tt extern "C"} means: this is a simple C function.

The C wrapper for {\tt hello\_world}, {\tt c\_hello\_world}, has one argument: {\tt err\_code}, 
which indicates whether the function finished successfully. It {\tt try}s the function {\tt hello\_world}, and 
sets the {\tt err\_code} depending on whether an error was {\tt throw}n (the C++ equivalent of {\tt raise}. 
When an error occurs, the error message is stored in the error handling module.

\cppUitleg{
\subsection{Input, Output and Inout arguments}
C++ can give you results as a return value, but that is not the only way in which 
results can be given. Earlier, we saw the return value {\tt char *string} from {\tt get\_error}: 
the result was written into the array that was passed by the user.

In {\tt c\_hello\_world}, we see another example: the argument {\tt err\_code}. It is declared as 
a pointer (see the star {\tt *} in its declaration), but it is not an array: it points only 
to one value, which can be written as {\tt err\_code[0]} or {\tt *err\_code}. C++ouPy uses
{\tt *err\_code} to avoid the confusion of inout arguments and arrays.

A pointer is used for the value {\tt err\_code} because C only passes arguments {\em by value}. 
The {\em value} of a pointer is a ... memory address!  A pointer is passed {\em by value}, a value 
is placed at the memory address, and effectively the pointed-to variable was passed {\em by reference}. 

This makes {\tt err\_code} in fact an inout-parameter: whatever the value is at the start of the 
function {\em may} have an effect on what the function does, or on the value that the argument has on 
exit. In this case, that is obviously not so, because the first line of the function is an initialization,
but C++ does not distinguish output arguments from inout arguments.
}

\subsection{Generated python code}
Finally, we look at the generated python code {\tt HelloWorld.py}. It starts with some imports:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
import re
import os
import sys
import numpy as np
from ctypes import c_long, c_size_t, c_double, c_int  # noqa
from coupy.coupling import Coupling, is_ndarray, loaded_dll, dll_runtime_error  # noqa
\end{lstlisting}
Note that the module {\tt coupy} is needed in the generated code. Therefore, 
the hybrid software will only work on computers where {\tt C++ouPy} is installed.
If installing {\tt C++ouPy} is not an option on a target machine, then {\tt C++ouPy} is not 
a good choice for your software.

After the includes come some initializing commands:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
dllname = os.path.join(os.path.dirname(__file__),'../../build/libC++ouPy-test.so')
dll = loaded_dll(dllname)
Coupling(os.path.join(os.path.dirname(__file__), '..','HelloWorld.json')).set_args_and_restype(dll)
\end{lstlisting}
Note that the path to the dll is made relative to the generated code, so the software will work after 
copying or moving, if the generated python code and the compiled code are moved together.

Finally, we get some functions:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
def runtime_error():
    return dll_runtime_error(dll)

def hello_world():
    err_code = np.zeros((1), dtype=c_int)
    dll.c_hello_world(err_code.ctypes.data)
    if err_code[0] != 1:
        raise(runtime_error())
    return
\end{lstlisting}
The function {\tt runtime\_error} makes it possible to raise errors coming from the dll, with the use 
of the error handling functions in the dll.
The funcion {\tt hello\_world}, finally, calls the wrapped function in the dll and handles the error:
it {\tt raise}s a {\tt RuntimeError} if the C++ code {\tt throws} an error, with the appropriate error
message.

Now we have seen the way 'Hello, world!' is coupled, we shall look into increasingly more complicated 
systems.
\clearpage
\section{Functions with inputs and outputs}
A simple example of some C++ functions that can be coupled with C++ouPy is shown in Table \ref{Tab: fibonacci c++}.
\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
header file {\tt fibonacci.h} &  source file {\tt fibnacci.cpp}\\ \hline
\begin{minipage}{6cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#ifndef FIBONACCI_H
#define FIBONACCI_H
#include <cstddef>
size_t fibonacci(const int i);
size_t array_size();
void fibonaccies(size_t fibo[]);
#endif
\end{lstlisting}
\end{minipage} & 
\begin{minipage}{11cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#include "fibonacci.h"
#include <stdexcept>

const size_t ARRAY_SIZE =17;

size_t fibonacci(const int i) {
    if (i<0) throw std::runtime_error("negative index");
    size_t retval = 1, nextval = 1;
    for (int j=0; j<i; j++) {
        const size_t hlp = retval;
        if (nextval < retval)
           throw std::runtime_error("size_t overflow"); 
        retval = nextval; nextval += hlp;
    }
    return retval;
}

size_t array_size() {return ARRAY_SIZE;}

void fibonaccies(size_t fibo[]) {
    fibo[0] = 1; fibo[1] = 1;
    for (size_t j=1; j<ARRAY_SIZE-1; j++) {
        fibo[j+1] = fibo[j] + fibo[j-1];
        if (fibo[j+1] < fibo[j])
           throw std::runtime_error("size_t overflow"); 
    }
}
\end{lstlisting}
\end{minipage} 
\\ \hline
\end{tabular}
\caption{\em C++ implementation of some Fibonacci functions.}
\label{Tab: fibonacci c++}
\end{table}
Note that the function {\tt fibonaccies} sets the first {\tt ARRAY\_SIZE} entries 
of the array {\tt fibo} without knowing its size. We will see later that this issue is
resolved in the generated python version of the function.

The coupling file is given in Table \ref{Tab: fibonacci json}
\begin{table}[htbp]
\begin{lstlisting}[language=Python,basicstyle=\ttfamily,columns=fullflexible]
{  "dll":       "../build/libC++ouPy.so",
   "hfiles":    ["\"fibonacci.h\""],
   "functions": [ { "name":   "array_size",  "output": "size_t narray"},
                    { "name":   "fibonacci",   "input":  ["const size_t i"],
                      "output": "size_t fibo_i"},
                    { "name":   "fibonaccies", "input":  ["size_t fibo[array_size()]"]} ] }
\end{lstlisting}
\caption{\em Coupling file {\tt tests/Fibonacci.json}}
\label{Tab: fibonacci json}
\end{table}
Note that the dimension of the result array {\tt fibo} (listed under {\tt inputs}) 
has been filled in.

The header file {\tt Fibonacci.h} declares the C-wrapper functions. After studying 
the header file {\tt HelloWorld.h}, 
there are no surprises here. Some of the lines in the header file are shown in 
Table \ref{Tab: fibonacci wrappers}.
\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
lines from header file {\tt Fibonacci.h} \\ \hline
\begin{minipage}{18cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
...
extern "C" size_t c_array_size(int *err_code);
extern "C" size_t c_fibonacci(const size_t i, int *err_code);
extern "C" void c_fibonaccies(size_t fibo[], int *err_code);
...
\end{lstlisting}
\end{minipage} \\ \hline
\end{tabular}
\caption{\em Lines from the wrapper file {\tt Fibonacci.h}}
\label{Tab: fibonacci wrappers}
\end{table}

The generated python function {\tt fibonaccies}, finally, is shown in Table \ref{Tab: generated fibonacci python}.
Now we see how the array is allocated: Python asks the dll how large the array must be and creates an array of the correct size.
\begin{table}[htbp]
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
def fibonaccies():
    fibo = np.zeros((array_size()), dtype=c_size_t)
    err_code = np.zeros((1), dtype=c_int)
    dll.c_fibonaccies(fibo.ctypes.data, err_code.ctypes.data)
    if err_code[0] != 1:
        raise(runtime_error())
    return fibo
\end{lstlisting}
\caption{\em One of the three generated functions in {\tt FibonacciC.py}.}
\label{Tab: generated fibonacci python}
\end{table}
\clearpage
\section{Coupling classes with C++ouPy}
An example of a C++ class is given in Tables \ref{Tab: Cantor header} and \ref{Tab: Cantor source}.
Thoigh there is quite some code, the class really is fairly simple:
it creates approximations of the Cantor set on the interval [0:1].
It starts with the entire interval [0:1], and then refines a certain number of times.
In each refinement, all intervals of the current approximation are cut in three equal parts, 
and the middle part is removed. The first refinement leaves us with the intervals [0:1/3] and [2/3:1],
the second one with [0:1/9], [2/9:1/3], [2/3:7/9], [8/9:1].

A lot of the code is of administrative nature: an empty constructor, a copy constructor, an overloaded assignment operator,
a print function, a destructor. Furthermore, there are {\em getters} ({\tt get\_n\_intervals} and {\tt get\_intervals}).
Really the only very interesting methods are {\tt init} and {\tt refine}. The nontrivial constructors consist of calls
to these two functions.

Experience with C++ouPy shows that all the administrative stuff usually ends up being necessary at some point in the 
development of the classes that need to be coupled to python. In cases where this is more work than it is worth,
C++ouPy may not be the right tool for your project.
\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
header file {\tt Cantor.h} \\ \hline
\begin{minipage}{18cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#ifndef CANTOR_H
#define CANTOR_H
#include <cstddef>
class Cantor {
 public:
   Cantor();
   Cantor(const Cantor &other);
   Cantor(const double max_interval_length);
   Cantor(const size_t n_refinements);
   Cantor(const double max_interval_length, const size_t n_refinements);
   ~Cantor();
   Cantor& operator=(const Cantor &other);
   size_t get_n_intervals() const;
   void get_intervals(double start_out[], double end_out[]) const;
   void refine();
   void print() const;
 private:
   void copy(const Cantor &other);
   void init();
   size_t n_intervals;
   double *start;
   double *end;
};
#endif
\end{lstlisting}
\end{minipage} \\ \hline
\end{tabular}
\caption{\em Header file for the class {\tt Cantor}}
\label{Tab: Cantor header}
\end{table}

\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
  source file {\tt Cantor.cpp}\\ \hline
\begin{minipage}{18cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#include "Cantor.h"
#include <cstdio>
#include <stdexcept>
Cantor::Cantor() { init(); }
Cantor::Cantor(const Cantor &other) { copy(other);}
Cantor::Cantor(const double max_interval_length) {
   init(); while (end[0] - start[0] > max_interval_length) refine();
}
Cantor::Cantor(const size_t n_refinements){
   init(); for(size_t j=0; j<n_refinements; j++) refine();
}
Cantor::Cantor(const double max_interval_length, const size_t n_refinements) {
   init(); for(size_t j=0; j<n_refinements; j++) refine();
   while (end[0] - start[0] > max_interval_length) refine();
}
Cantor::~Cantor() { delete[] start; delete[] end;}
Cantor& Cantor::operator=(const Cantor &other) {copy(other); return *this;}
size_t Cantor::get_n_intervals() const {return n_intervals;}
void Cantor::get_intervals(double start_out[], double end_out[]) const {
    for (size_t j =0; j<n_intervals; j++) {
        start_out[j] = start[j]; end_out[j] = end[j];
    }
}
void Cantor::refine(){
    double *start_new = new double[2*n_intervals], *end_new = new double[2*n_intervals];
    for (size_t j =0; j<n_intervals; j++) {
       start_new[2*j] = start[j];                 end_new[2*j]   = (2*start[j]+end[j])/3;
       start_new[2*j+1] = (start[j]+2*end[j])/3;  end_new[2*j+1]   = end[j];
    }
    delete[] start; delete[] end;
    end = end_new; start = start_new; n_intervals *=2;
}
void Cantor::print() const {
    printf("\n");
    for (size_t j =0; j<n_intervals; j++)
        printf("  interval %d: %0.3f:%0.3f\n", (int) j, start[j], end[j]);
    printf("\n");
}
void Cantor::copy(const Cantor &other) {
   n_intervals = other.n_intervals; start = new double [n_intervals];
   end = new double [n_intervals];
   for (size_t j =0; j<n_intervals; j++) {start[j] = other.start[j]; end[j]=other.end[j];}
}
void Cantor::init() {
 n_intervals = 1; start = new double[1]; end = new double[1]; start[0] = 0; end[0] =1;
}
\end{lstlisting}
\end{minipage} 
\\ \hline
\end{tabular}
\caption{\em Source file {\tt Cantor.cpp}}
\label{Tab: Cantor source}
\end{table}
The coupling is established using the coupling script {\tt CantorCouple.json}, shown in Table \ref{Tab: Cantor json}.
\begin{table}[htbp]
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
{ "dll":       "../build/libC++ouPy.so",
   "classes":
   [ { "class": "Cantor",
       "create": ["const double max_interval_length", "const size_t n_refinements"],
       "methods": [
            { "name": "get_n_intervals", "output": "size_t n_intervals" }, 
            { "name": "get_intervals",   "input": ["double start[self.get_n_intervals()]",
                                                   "double end[self.get_n_intervals()]"]},
            { "name": "refine", "const": false},
            { "name": "print"}
] } ] }
\end{lstlisting}
\caption{\em The script {\tt CantorCouple.json}, used to couple the class {\tt Cantor} to python.}
\label{Tab: Cantor json}.
\end{table}
The generated wrapper code is shown in Table \ref{Tab: Cantor wrappers}.
\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
header file {\tt CantorCouple.h} \\ \hline 
\begin{minipage}{17cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
...
    extern "C" Cantor* Cantor_Create(const double max_interval_length,
                                     const size_t n_refinements, int *err_code);
    extern "C" size_t Cantor_get_n_intervals(const Cantor* C, int *err_code);
    extern "C" void Cantor_get_intervals(const Cantor* C, double start[], double end[],
                                         int *err_code);
    extern "C" void Cantor_refine(Cantor* C, int *err_code);
    extern "C" void Cantor_print(const Cantor* C, int *err_code);
    extern "C" void Cantor_delete(Cantor *C);
...
\end{lstlisting}
\end{minipage} \\ \hline
\multicolumn{1}{c}{}\\[1ex] \hline
source file {\tt CantorCouple.cpp}\\ \hline
\begin{minipage}{17cm}
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
...
Cantor* Cantor_Create(const double max_interval_length, const size_t n_refinements,
                      int *err_code) {
     *err_code=1;
     try {
        Cantor *C = new Cantor(max_interval_length, n_refinements);
        fflush(stdout);
        return C;
     } catch (const std::exception &e) {
        set_error(e.what());
        *err_code=0;
        return (Cantor*) NULL;
     }
}
...
void Cantor_delete(Cantor *C){
    delete C;
}
\end{lstlisting}
\end{minipage} 
\\ \hline
\end{tabular}
\caption{\em Some lines of wrapper files {\tt CantorCouple.h} and {\tt CantorCouple.cpp}}
\label{Tab: Cantor wrappers}
\end{table}
It is interesting to see that the {\tt Create}-function allocates an object and returns a pointer to it.
The destructor {\tt deletes} the pointer, which automatically activates the destructor.

All member functions (methods) have the object pointer as the first argument, and have the 
class name prepended to the name of the wrapper function. Whenever possible, 
the object pointer is declared {\tt const}.

\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
generated file {\tt CantorC.py} \\ \hline 
\begin{minipage}{17cm}
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
import ...
class CantorC():
    def __del__(self):
        if self._Cmine:
            dll.Cantor_delete(self._C)

    def __init__(self, max_interval_length, n_refinements):
        err_code = np.zeros((1), dtype=c_int)
        self._C = dll.Cantor_Create(
            max_interval_length, n_refinements, err_code.ctypes.data)
        if err_code[0] != 1:
            raise(runtime_error())
        self._Cmine = True
...
\end{lstlisting}
\end{minipage} \\ \hline
\end{tabular}
\caption{\em Some lines of the generated python file {\tt CantorC.py}}
\label{Tab: CantorC python }
\end{table}
Of the various constructors, only one makes it into the python-version {\tt CantorC}.

The class {\tt CantorC} is not the final version of the {\tt Cantor} class in python. 
The inherited class {\tt Cantor} is shown in Table \ref{Tab: Cantor python}.

\begin{table}[htbp]
\begin{tabular}{|l|l|}
\hline
generated file {\tt CantorC.py} \\ \hline 
\begin{minipage}{17cm}
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
class Cantor(CantorC):
    def __init__(self, max_interval_length=1, n_refinements=0):
        print("Calling CantorC's __init__")
        CantorC.__init__(self, max_interval_length, n_refinements)

    def plot_contours(self):
        start, end = self.get_intervals()
        sqrt3 = np.sqrt(3.0)
        x = [None] * (2*start.size)
        y = [None] * (2*start.size)
        c = [None] * (2*start.size)
        x[-1::-2] = [ [-0.5*s, 0.5*s, 0, -0.5*s] for s in start]
        x[-2::-2] = [ [-0.5*e, 0.5*e, 0, -0.5*e] for e in end]
        y[-1::-2] = [ [sqrt3*s/6, sqrt3*s/6, -sqrt3*s/3, sqrt3*s/6 ] for s in start]
        y[-2::-2] = [ [sqrt3*e/6, sqrt3*e/6, -sqrt3*e/3, sqrt3*e/6 ] for e in end]
        c[-1::-2] = [ 'b'  for s in start]
        c[-2::-2] = [ 'r' for e in end]
        return x, y, c
\end{lstlisting}
\end{minipage} \\ \hline
\end{tabular}
\caption{\em Some lines of the python file {\tt Cantor.py}}
\label{Tab: Cantor python}
\end{table}
Just like the C++ class, the python class {\tt Cantor} can be constructed from just 
the {\tt max\_interval\_length}, the {\tt n\_refinements}, or both, because of the use 
of default values in python.

The python class {\tt Cantor} has all the coupled member functions, plus one that exists only
in python: the {\tt plot} function, which collects the information needed to create a pretty 
picture like the one shown in Figure \ref{Fig: Cantor}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=10cm]{figs/Cantor.pdf}
\end{center}
\caption{\em A visualization of the {\tt Cantor} class, made using the member function {\tt plot}.}
\label{Fig: Cantor}
\end{figure}
This shows how the hybrid {\tt Cantor} class consists of C++ and python elements, and how they connect 
almost seamlessly.
\end{document}
